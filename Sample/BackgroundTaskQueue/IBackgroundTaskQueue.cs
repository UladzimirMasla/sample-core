﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

using Sample.DataAccess.Models;

namespace Sample.BackgroundTaskQueue
{
    interface IBackgroundTaskQueue
    {
        ValueTask QueueBackgroundWorkItemAsync(Donation data);

        ValueTask<Donation> DequeueAsync(CancellationToken cancellationToken);

        IAsyncEnumerable<Donation> DequeueAllAsync(CancellationToken cancellationToken);
    }
}
