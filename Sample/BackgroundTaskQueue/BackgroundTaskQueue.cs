﻿using System;
using System.Threading.Channels;
using System.Threading;
using System.Threading.Tasks;
using Sample.DataAccess.Models;
using System.Collections.Generic;

namespace Sample.BackgroundTaskQueue
{
    public class Queue : IBackgroundTaskQueue
    {
        private readonly Channel<Donation> _queue;


        public Queue(int capacity)
        {
            // Capacity should be set based on the expected application load and
            // number of concurrent threads accessing the queue.
            // BoundedChannelFullMode.Wait will cause calls to WriteAsync() to return a task,
            // which completes only when space became available. This leads to backpressure,
            // in case too many publishers/calls start accumulating.
            var options = new BoundedChannelOptions(capacity)
            {
                FullMode = BoundedChannelFullMode.Wait
            };

            _queue = Channel.CreateBounded<Donation>(options);
        }


        public async ValueTask QueueBackgroundWorkItemAsync(Donation data)
        {
            if (data == null)
            {
                throw new ArgumentNullException(nameof(data));
            }

            await _queue.Writer.WriteAsync(data);
        }

        public async ValueTask<Donation> DequeueAsync(
            CancellationToken cancellationToken)
        {
            var data = await _queue.Reader.ReadAsync(cancellationToken);

            return data;
        }

        public IAsyncEnumerable<Donation> DequeueAllAsync(CancellationToken cancellationToken)
        {
            var data = _queue.Reader.ReadAllAsync(cancellationToken);

            return data;
        }
    }
}
