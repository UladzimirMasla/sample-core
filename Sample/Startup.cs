using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.AspNetCore.Routing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

using Sample.BackgroundTaskQueue;
using Sample.DataAccess;
using Sample.Filters;
using Sample.Middlewares;
using Sample.Options;
using System;

namespace Sample
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<EmailSenderOptions>(Configuration.GetSection(
                                        EmailSenderOptions.SectionName));
            services.Configure<ThanksgivingSettings>(Configuration.GetSection(
                                        ThanksgivingSettings.SectionName));
            services.Configure<TestSectionOptions>(Configuration.GetSection(
                                        TestSectionOptions.SectionName));
            services.Configure<CustomHeaderOptions>(Configuration.GetSection(
                                        CustomHeaderOptions.SectionName));

            var connection = Configuration.GetConnectionString("DefaultConnection");
            services.AddDbContext<ApplicationContext>(options => options.UseSqlServer(connection));
            services.AddSingleton<IBackgroundTaskQueue>(x => new Queue(10));

            services.AddControllersWithViews()
                .AddXmlSerializerFormatters()
                .AddMvcOptions(options =>
                {
                    options.ModelMetadataDetailsProviders.Add(
                        new SuppressChildValidationMetadataProvider(typeof(Guid)));

                    options.Filters.Add(typeof(AddHeaderAttribute));
                    options.Filters.Add(typeof(CustomExceptionFilter));
                    options.Filters.Add(typeof(ValidateModelAttribute));
                });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env,
            ILogger<Startup> logger)
        {
            if (env.IsDevelopment())
            {
                logger.LogInformation("In Development.");
                app.UseDeveloperExceptionPage();
            }
            else
            {
                logger.LogInformation("Not Development.");
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseMiddleware<ErrorLoggingMiddleware>();
            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.Use(next => context =>
            {
                var endpoint = context.GetEndpoint();
                if (endpoint?.Metadata.GetMetadata<AuditPolicyAttribute>() != null)
                {
                    logger.LogInformation("Attempt to reach endpoint marked with AuditPolicyAttribute at {time}. Metadata count = {metadatacount}", DateTime.UtcNow, endpoint?.Metadata.Count);
                }


                return next(context);
            });

            app.UseRouting();
            // Routing zone

            app.Use(next => context =>
            {
                var endpoint = context.GetEndpoint();

                if(endpoint == null)
                {
                    // Routing found no endpoint to send request to
                    return next(context);
                }

                var route = endpoint is RouteEndpoint routeEndpoint ? routeEndpoint.RoutePattern : null;
                var metadata = endpoint.Metadata;

                if (endpoint?.Metadata.GetMetadata<AuditPolicyAttribute>() != null)
                {
                    logger.LogInformation("Attempt to reach endpoint marked with AuditPolicyAttribute at {time}. Metadata count = {metadatacount}", DateTime.UtcNow, endpoint?.Metadata.Count);
                }


                return next(context);
            });

            app.UseAuthorization();
            // almost same as UseAuthorization above :)
            app.Use(next => context =>
            {
                var endpoint = context.GetEndpoint();

                if (endpoint == null)
                {
                    return next(context);
                }

                // Allow Anonymous skips all authorization
                if (endpoint?.Metadata.GetMetadata<IAllowAnonymous>() != null)
                {
                    return next(context);
                }

                var authorizeData = endpoint?.Metadata.GetOrderedMetadata<IAuthorizeData>() ?? Array.Empty<IAuthorizeData>();

                // check authorizeData
                // u can terminate execution here if authorizeData check failed
                // but check itself is error-prone and not recommended to made by one


                // so suppose everything is cool 
                return next(context);
            });

            // Routing zone end
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");

                endpoints.MapControllerRoute(
                    name: "Catch-all route",
                    pattern: "{controller=Home}/{action=Index}/Catch/{**stuff}");
            });
        }
    }

    public class AuditPolicyAttribute : Attribute
    {
        public AuditPolicyAttribute(bool needsAudit)
        {
            NeedsAudit = needsAudit;
        }

        public bool NeedsAudit { get; }
    }
}
