﻿using Microsoft.AspNetCore.Mvc;
using Sample.Models;

namespace Sample.Controllers
{
    public class ModelsController : Controller
    {
        [BindProperty(SupportsGet = true)]
        public UserInfo UserInfo { get; set; }

        [BindProperty(Name = "Info", SupportsGet = true)]
        public string SomeInfo { get; set; }

        public string SomeOtherInfo { get; set; }


        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public IActionResult GetInfo()
        {
            var responce = $"{UserInfo?.Name}, {UserInfo?.Age}. {nameof(SomeInfo)} = {SomeInfo}, {nameof(SomeOtherInfo)} = {SomeOtherInfo}";

            return new OkObjectResult(responce);
        }


        [HttpPost]
        public IActionResult ProcessInfo(UserInfo info)
        {
            if(!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            return new OkObjectResult(info);
        }

        [HttpPost]
        [Consumes("application/json")]
        public IActionResult ProcessInfo2([FromBody] UserInfo info)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            return new OkObjectResult(info);
        }
    }
}
