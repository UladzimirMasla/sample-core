﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

using Sample.DataAccess;
using Sample.DataAccess.Models;
using System;

namespace Sample.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly ApplicationContext _context;

        public HomeController(ILogger<HomeController> logger, ApplicationContext context)
        {
            _logger = logger;
            _context = context;
        }

        public IActionResult Index(int? id)
        {
            return View();
        }

        public IActionResult Error()
        {
            throw new NotImplementedException();

            return View();
        }

        [HttpGet]
        public IActionResult Donate()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Donate(Donation donation)
        {
            _context.Donations.Add(donation);
            _context.SaveChanges();

            return View();
        }
    }
}
