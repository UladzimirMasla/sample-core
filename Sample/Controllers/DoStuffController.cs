﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

using System;

namespace Sample.Controllers
{
    public class DoStuffController : Controller
    {
        private ILogger<DoStuffController> _logger;


        public DoStuffController(ILogger<DoStuffController> logger)
        {
            _logger = logger;
        }


        [HttpGet]
        public IActionResult Index(string? stuff)
        {
            var some = stuff;

            return View();
        }

        [HttpPost]
        public IActionResult Index(int? number)
        {
            _logger.LogInformation("Begin post Index method execution");

            if (number == null)
            {
                _logger.LogWarning("Expected argument number");
            }
            else
            {
                DoStuffWithNumber(number.Value);
            }

            return RedirectToAction(nameof(Index));
        }


        private void DoStuffWithNumber(int number)
        {
            _logger.LogInformation("Begin DoStuffWithNumber method execution");
            _logger.LogInformation("Got number={number}", number);

            if (number < 0)
            {
                _logger.LogError("number should be greater than zero");

                throw new ArgumentException("number should be greater than zero", nameof(number));
            }
            if(number > 1000)
            {
                _logger.LogWarning("number is too large, can cause some calculation issues");
            }


            var rand = new Random();
            var multiplier = rand.Next(100);

            var thirdPartyArgument = rand.Next(100);

            if(thirdPartyArgument > 40 && thirdPartyArgument < 70)
            {
                _logger.LogWarning("Third-party took too long to respond, greater response time can cause an error. Current response time = {0}ms", thirdPartyArgument);
            }
            else if(thirdPartyArgument > 70)
            {
                _logger.LogError("Shutting down third-party call due to long response time. Current response time={0}ms", thirdPartyArgument);

                throw new TimeoutException("Third-party took too long to respond");
            }

            _logger.LogInformation("Retrieved argument from third-party={0}", thirdPartyArgument);
            _logger.LogTrace("First multiplier={0}", multiplier);

            var newNumber = number * rand.Next(100);

            _logger.LogInformation("New number={0}", newNumber);

            var oldPlusNew = newNumber + number;

            _logger.LogDebug("Old + new={0}", oldPlusNew);

            var work = Math.Log10(Math.Round(newNumber * Math.PI + Math.Pow(oldPlusNew, 3))) + thirdPartyArgument;

            _logger.LogInformation("End of DoStuffWithNumber method execution, Result={0}", work);
        }
    }
}
