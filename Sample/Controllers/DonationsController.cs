﻿using System.Linq;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

using Sample.DataAccess;

namespace Sample.Controllers
{
    [AuditPolicy(false)]
    public class DonationsController : Controller
    {
        private ApplicationContext _context;
        private ILogger<DonationsController> _logger;

        public DonationsController(ApplicationContext context, ILogger<DonationsController> logger)
        {
            _context = context;
            _logger = logger;
        }


        public ActionResult Index()
        {
            _logger.LogTrace("Begin retrieving Donations");

            var donations = _context.Donations.ToList();

            _logger.LogDebug("Retrieved {amount} donations", donations.Count);

            return View(donations);
        }

        public async Task<IActionResult> Delete(int? id)
        {
            var donation = await _context.Donations.FindAsync(id);

            if(donation == null)
            {
                _logger.LogWarning("Donation with id={donationId} not found", id);
            }
            else
            {
                _logger.LogTrace("Deleting donation id={donationId}", id);
                _context.Donations.Remove(donation);

                _context.SaveChanges();
            }

            return RedirectToAction(nameof(Index));
        }
    }
}
