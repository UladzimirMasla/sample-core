﻿namespace Sample.Options
{
    public class CustomHeaderOptions
    {
        public const string SectionName = "CustomHeader";

        public string HeaderName { get; set; }

        public string HeaderValue { get; set; }
    }
}
