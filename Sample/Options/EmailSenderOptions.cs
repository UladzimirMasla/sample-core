﻿namespace Sample.Options
{
    public class EmailSenderOptions
    {
        public const string SectionName = "EmailSender";

        public string SmtpEmail { get; set; }

        public string SmtpPassword { get; set; }

        public string SmtpServiceHost { get; set; }
    }
}
