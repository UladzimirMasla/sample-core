﻿namespace Sample.Options
{
    public class ThanksgivingSettings
    {
        public const string SectionName = "ThanksgivingSettings";

        public int ThanksgivingAmount { get; set; }
    }
}
