﻿using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Sample
{
    public class TestService : BackgroundService
    {
        private ILogger<TestService> _logger;


        public TestService(ILogger<TestService> logger)
        {
            _logger = logger;
        }


        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            while(!stoppingToken.IsCancellationRequested)
            {
                _logger.LogInformation("TestService tick at {time}", DateTime.Now);
                await Task.Delay(1000);
            }
        }
    }
}
