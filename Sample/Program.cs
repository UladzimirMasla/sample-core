using System;
using System.IO;
using System.Linq;

using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using NLog;
using NLog.Web;
using Sample.DataAccess;
using Sample.Options;

namespace Sample
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var logger = NLogBuilder.ConfigureNLog("nlog.config").GetCurrentClassLogger();
            try
            {
                logger.Info("Initializing main");

                var environment = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
                logger.Info("Current environment: {environment}", environment);
                if(environment == Environments.Development)
                {
                    logger.Info("Current environment: {environment}", environment);
                }

                var host = CreateHostBuilder(args).Build();


                ApplicationContext context = null;

                using (var scope = host.Services.CreateScope())
                {
                    context = scope.ServiceProvider.GetService<ApplicationContext>();
                    var options = scope.ServiceProvider.GetService<IOptionsSnapshot<TestSectionOptions>>();

                    var donationsFSC = context.Donations.ToList();
                }

                //var donations = context.Donations.ToList();
                //
                host.Run();
            }
            catch (Exception exception)
            {
                logger.Error(exception, "Stopped program because of exception");
                throw;
            }
            finally
            {
                LogManager.Shutdown();
            }
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureHostConfiguration(configuration =>
                {
                    configuration.SetBasePath(Directory.GetCurrentDirectory());
                    configuration.AddEnvironmentVariables(prefix: "PREFIX_");
                    configuration.AddCommandLine(args);
                })
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                })
                .ConfigureAppConfiguration((hostingContext, config) =>
                {
                    var environment = hostingContext.HostingEnvironment;

                    config.AddJsonFile("MyConfig.json", optional: true, reloadOnChange: true)
                    .AddJsonFile($"MyConfig.{environment.EnvironmentName}.json",
                                     optional: true, reloadOnChange: true); ;
                })
                .ConfigureServices(services =>
                {
                    services.AddHostedService<DonationAnalizerService>();
                    services.AddHostedService<EmailSenderService>();
                    //services.AddHostedService<TestService>();
                })
                .UseNLog();

        //.UseSerilog((context, services, configuration) => configuration
        //    .ReadFrom.Configuration(context.Configuration)
        //    .MinimumLevel.Information()
        //    .ReadFrom.Services(services)
        //    .Enrich.FromLogContext()
        //    .WriteTo.TeleSink(
        //       telegramApiKey: "1880576886:AAE0XYuK9-NTp_NSDmFGnVvIQAw_aQI6GZo",
        //       telegramChatId: "501784387"));
    }
}
