﻿using Microsoft.EntityFrameworkCore;

using Sample.DataAccess.Models;

namespace Sample.DataAccess
{
    public class ApplicationContext : DbContext
    {
        public DbSet<Donation> Donations { get; set; }

        public DbSet<CompletedDonationAlert> CompletedDonationAlerts { get; set; }


        public ApplicationContext(DbContextOptions<ApplicationContext> options)
            :base(options)
        {
            Database.EnsureCreated();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder
                .Entity<Donation>()
                .HasOne(d => d.CompletedDonationAlert)
                .WithOne(a => a.Donation)
                .HasForeignKey<CompletedDonationAlert>(a => a.DonationId);
        }
    }
}
