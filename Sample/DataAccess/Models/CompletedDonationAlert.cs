﻿namespace Sample.DataAccess.Models
{
    public class CompletedDonationAlert
    {
        public int Id { get; set; }

        public int DonationId { get; set; }

        public Donation Donation { get; set; }
    }
}
