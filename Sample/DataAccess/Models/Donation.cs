﻿namespace Sample.DataAccess.Models
{
    public class Donation
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Email { get; set; }

        public double Amount { get; set; }

        public CompletedDonationAlert CompletedDonationAlert { get; set; }
    }
}
