﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.Logging;

using MimeKit;
using MailKit.Net.Smtp;
using MailKit.Security;

using Sample.BackgroundTaskQueue;
using Sample.DataAccess;
using Sample.DataAccess.Models;
using Sample.Options;

namespace Sample
{
    public class EmailSenderService : IHostedService, IDisposable
    {
        private IServiceProvider _services;
        private ILogger<EmailSenderService> _logger;
        private Timer _timer;


        public EmailSenderService(IServiceProvider services,
            ILogger<EmailSenderService> logger)
        {
            _services = services;
            _logger = logger;
        }


        public async Task StartAsync(CancellationToken cancellationToken)
        {
            _timer = new Timer(ExecuteInternalAsync, null, TimeSpan.Zero, TimeSpan.FromSeconds(60));
        }

        public async Task StopAsync(CancellationToken cancellationToken)
        {
            _timer?.Change(Timeout.Infinite, 0);
        }

        public void Dispose()
        {
            _timer?.Dispose();
        }


        private void ExecuteInternalAsync(object state)
        {
            using (var scope = _services.CreateScope())
            {
                var context =
                    scope.ServiceProvider
                        .GetRequiredService<ApplicationContext>();

                var backgroundTaskQueue =
                    scope.ServiceProvider
                        .GetRequiredService<IBackgroundTaskQueue>();

                var options =
                    scope.ServiceProvider
                        .GetRequiredService<IOptionsSnapshot<EmailSenderOptions>>()
                        .Value;

                var emailJob = SendEmailAsync(context, backgroundTaskQueue, options);

                Task.WaitAll(emailJob);
            }
        }


        private async Task SendEmailAsync(ApplicationContext context, IBackgroundTaskQueue queue, EmailSenderOptions options)
        {
            await foreach (var donation in queue.DequeueAllAsync(new CancellationToken()))
            {
                _logger.LogDebug($"Dequeued donation with {donation.Id} id");

                if (IsRequiredToSend(context, donation))
                {
                    await SendEmailInternalAsync(donation.Email, options);
                    _logger.LogInformation("Thanks send to {Email}", donation.Email);

                    await MarkDonationAsSend(donation, context);
                    _logger.LogInformation("Donation({DonationId}) added to CompletedDonationAlerts", donation.Id);
                }
                else
                {
                    _logger.LogWarning($"Donation with {donation.Id} id is not required to send");
                }
            }
        }

        private bool IsRequiredToSend(ApplicationContext context, Donation donation)
        {
            return context.CompletedDonationAlerts.All(alert => alert.DonationId != donation.Id);
        }

        private async Task SendEmailInternalAsync(string emailToSendTo, EmailSenderOptions options)
        {
            var emailMessage = new MimeMessage();

            emailMessage.From.Add(new MailboxAddress("Product owner", "login@yandex.ru"));
            emailMessage.To.Add(new MailboxAddress("", emailToSendTo));
            emailMessage.Subject = "Donation you made.";
            emailMessage.Body = new TextPart(MimeKit.Text.TextFormat.Html)
            {
                Text = "Thanks for your donation!"
            };

            using (var client = new SmtpClient())
            {
                await client.ConnectAsync(options.SmtpServiceHost, 587, SecureSocketOptions.StartTls);
                await client.AuthenticateAsync(options.SmtpEmail, options.SmtpPassword);
                await client.SendAsync(emailMessage);

                await client.DisconnectAsync(true);
            }
        }

        private async Task MarkDonationAsSend(Donation donation, ApplicationContext context)
        {
            context.CompletedDonationAlerts.Add(new CompletedDonationAlert
            {
                DonationId = donation.Id
            });

            await context.SaveChangesAsync();
        }
    }
}
