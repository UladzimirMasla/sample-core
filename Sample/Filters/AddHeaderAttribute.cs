﻿using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Options;

using Sample.Options;

namespace Sample.Filters
{
    public class AddHeaderAttribute : ResultFilterAttribute
    {
        private readonly CustomHeaderOptions _options;


        public AddHeaderAttribute(IOptionsSnapshot<CustomHeaderOptions> options)
        {
            _options = options.Value;
        }


        public override void OnResultExecuting(ResultExecutingContext context)
        {
            context.HttpContext.Response.Headers.Add(_options.HeaderName, new string[] { _options.HeaderValue });

            base.OnResultExecuting(context);
        }
    }
}
