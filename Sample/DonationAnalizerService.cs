﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

using Sample.BackgroundTaskQueue;
using Sample.DataAccess;
using Sample.Options;

namespace Sample
{
    public class DonationAnalizerService : BackgroundService
    {
        private IServiceProvider _services;
        private ILogger<DonationAnalizerService> _logger;


        public DonationAnalizerService(IServiceProvider services,
            ILogger<DonationAnalizerService> logger)
        {
            _services = services;
            _logger = logger;
        }


        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            _logger.LogInformation("Start of method execution");

            while(!stoppingToken.IsCancellationRequested)
            {
                await Task.Delay(TimeSpan.FromSeconds(30), stoppingToken);
                QueueLargeDonationToSendEmail();
            }
        }


        private void QueueLargeDonationToSendEmail()
        {
            using (var scope = _services.CreateScope())
            {
                var context =
                    scope.ServiceProvider
                        .GetRequiredService<ApplicationContext>();

                var backgroundTaskQueue =
                    scope.ServiceProvider
                        .GetRequiredService<IBackgroundTaskQueue>();

                var settings =
                    scope.ServiceProvider
                        .GetRequiredService<IOptions<ThanksgivingSettings>>()
                        .Value;

                var task = QueueLargeDonationToSendEmailInternalAsync(context, backgroundTaskQueue, settings);

                Task.WaitAll(task);
            }
            
        }


        private async Task QueueLargeDonationToSendEmailInternalAsync(ApplicationContext context, IBackgroundTaskQueue queue, ThanksgivingSettings settings)
        {
            if(settings.ThanksgivingAmount < 1)
            {
                throw new ArgumentException("Please, provide correct ThanksgivingAmount value in configuration", "ThanksgivingSettings.ThanksgivingAmount");
            }

            var processedDonationsIds = context.CompletedDonationAlerts
                .Select(alert => alert.DonationId)
                .ToList();

            var largeDonationsToProcess = context.Donations
                .Where(donation => 
                    donation.Amount > settings.ThanksgivingAmount &&
                    !processedDonationsIds.Contains(donation.Id))
                .ToList();

            var toProcessCount = largeDonationsToProcess.Count;
            if(toProcessCount > 0)
            {
                _logger.LogDebug($"Found {largeDonationsToProcess.Count} donation to process");
            }

            foreach (var donation in largeDonationsToProcess)
            {
                _logger.LogDebug($"Adding donation with id {donation.Id} to queue");

                await queue.QueueBackgroundWorkItemAsync(donation);
            }
        }
    }
}
