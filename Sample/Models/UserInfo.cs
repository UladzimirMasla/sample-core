﻿
using System.ComponentModel.DataAnnotations;

namespace Sample.Models
{
    public class UserInfo
    {
        public int Id { get; set; }

        //[Required(AllowEmptyStrings = false, ErrorMessage = "Name property required")]
        public string Name { get; set; }

        public int Age { get; set; }
    }
}
